package test

import (
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestServer(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{
		TerraformDir: "../examples",
		VarFiles:     []string{"terraform.tfvars"},
		BackendConfig: map[string]interface{}{
			"username": os.Getenv("TF_USERNAME"),
			"password": os.Getenv("ACCESS_TOKEN"),
		},
	}

	defer terraform.Destroy(t, terraformOptions)
	terraform.InitAndApply(t, terraformOptions)

	server_id := terraform.Output(t, terraformOptions, "id")
	server_ip := terraform.Output(t, terraformOptions, "ipv4_address")
	server_network := terraform.Output(t, terraformOptions, "network")

	assert.NotEmpty(t, server_id, "server_id was empty")
	assert.NotEmpty(t, server_ip, "server does not have ip assigned")
	assert.NotEmpty(t, server_network, "server network setup correctly")
}
