<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.0 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | >= 1.33.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | >= 1.33.2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [hcloud_server.this](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/server) | resource |
| [hcloud_volume.this](https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/volume) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_automount"></a> [automount](#input\_automount) | (Optional, bool) Automount the volume upon attaching it (server\_id must be provided). | `bool` | `true` | no |
| <a name="input_backups"></a> [backups](#input\_backups) | Wether backups should be enabled or not | `bool` | `false` | no |
| <a name="input_create_volume"></a> [create\_volume](#input\_create\_volume) | Whether volumes should be created on server creation | `bool` | `false` | no |
| <a name="input_datacenter"></a> [datacenter](#input\_datacenter) | (Optional, string) The datacenter name to create the server in. | `string` | `""` | no |
| <a name="input_delete_protection"></a> [delete\_protection](#input\_delete\_protection) | (Optional, bool) Enable or disable delete protection. | `bool` | `false` | no |
| <a name="input_image_name"></a> [image\_name](#input\_image\_name) | (Required, string) Name or ID of the image the server is created from. Note the image property is only required when using the resource to create servers. As the Hetzner Cloud API may return servers without an image ID set it is not marked as required in the Terraform Provider itself. Thus, users will get an error from the underlying client library if they forget to set the property and try to create a server. | `string` | n/a | yes |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to assign to server | `map(string)` | `{}` | no |
| <a name="input_location"></a> [location](#input\_location) | (Optional, string) The location name to create the server in. nbg1, fsn1, hel1 or ash | `string` | `"nbg1"` | no |
| <a name="input_network_id"></a> [network\_id](#input\_network\_id) | (Required) ID of the network | `number` | n/a | yes |
| <a name="input_public_ips"></a> [public\_ips](#input\_public\_ips) | Whether the server gets assigned a public IP or not | `bool` | `true` | no |
| <a name="input_server_name"></a> [server\_name](#input\_server\_name) | (Required, string) Name of the server to create (must be unique per project and a valid hostname as per RFC 1123). | `string` | n/a | yes |
| <a name="input_server_type"></a> [server\_type](#input\_server\_type) | (Required, string) Name of the server type this server should be created with. | `string` | n/a | yes |
| <a name="input_ssh_keys"></a> [ssh\_keys](#input\_ssh\_keys) | (Optional, list) SSH key IDs or names which should be injected into the server at creation time | `list(string)` | `[]` | no |
| <a name="input_user_data"></a> [user\_data](#input\_user\_data) | (Optional, string) Cloud-Init user data to use during server creation | `string` | `""` | no |
| <a name="input_volume_format"></a> [volume\_format](#input\_volume\_format) | (Optional, string) Format volume after creation. xfs or ext4 | `string` | `"ext4"` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | (Required, int) Size of the volume (in GB). | `number` | `20` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | (int) Unique ID of the server. |
| <a name="output_image"></a> [image](#output\_image) | (string) Name or ID of the image the server was created from. |
| <a name="output_ipv4_address"></a> [ipv4\_address](#output\_ipv4\_address) | (string) The IPv4 address. |
| <a name="output_name"></a> [name](#output\_name) | (string) Name of the server. |
| <a name="output_network"></a> [network](#output\_network) | (map) Private Network the server shall be attached to. The Network that should be attached to the server requires at least one subnetwork. Subnetworks cannot be referenced by Servers in the Hetzner Cloud API. Therefore Terraform attempts to create the subnetwork in parallel to the server. This leads to a concurrency issue. It is therefore necessary to use depends\_on to link the server to the respective subnetwork |
<!-- END_TF_DOCS -->
