## [1.3.2](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.3.1...1.3.2) (2023-05-09)


### Bug Fixes

* updated server labels ([8de0cea](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/8de0cea30f5b8010da85fb504450c95d30523520))

## [1.3.1](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.3.0...1.3.1) (2023-04-13)


### Bug Fixes

* disable backups by default ([9038bc2](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/9038bc2cc970186019b9765c52af1334a078e183))

# [1.3.0](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.12...1.3.0) (2023-04-04)


### Features

* removed firewall from server module ([3f94e8e](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/3f94e8ec56b409eb6e48cab6ce4fd2ec6febc573))

## [1.2.12](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.11...1.2.12) (2023-04-03)


### Bug Fixes

* output firewall id ([0d07c7c](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/0d07c7cd9a29e27624813c44a2706391be21a051))

## [1.2.11](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.10...1.2.11) (2023-04-03)


### Bug Fixes

* added lifecycle changes ([236c2fc](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/236c2fc17300bb1350e6346db291967aab6d4924))

## [1.2.10](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.9...1.2.10) (2023-04-03)


### Bug Fixes

* trigger new version ([a8bec40](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/a8bec400acf3773783ee13a2699209e038a2c6cf))

## [1.2.9](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.8...1.2.9) (2023-02-10)


### Bug Fixes

* added labels ([022a247](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/022a24747b65940fb0822ec0f3eff0ce0f8ab28c))

## [1.2.8](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.7...1.2.8) (2023-01-17)


### Bug Fixes

* made volumes optional ([72b0ffc](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/72b0ffc1cb7f463aecbf1b6abe9e38aba7e44ca2))

## [1.2.7](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.6...1.2.7) (2023-01-03)


### Bug Fixes

* added backup option ([1eafcae](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/1eafcaeb01a6b273e71cf8ef731d56506354d8a5))

## [1.2.6](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.5...1.2.6) (2022-12-27)


### Bug Fixes

* added protocol option to firewall rule ([6719351](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/6719351afb7b2382eb36f6843d005760dcd472bc))

## [1.2.5](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.4...1.2.5) (2022-12-27)


### Bug Fixes

* added terratest ([fe42aec](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/fe42aecaf4ad45b903cf4a48bc2d91454ae43892))

## [1.2.4](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.3...1.2.4) (2022-12-13)


### Bug Fixes

* made firewall optional ([abcf06a](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/abcf06a2d27bf5768660ffe3d16eba03acf644dc))

## [1.2.3](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.2...1.2.3) (2022-12-12)


### Bug Fixes

* added more firewall wait time ([dbedf00](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/dbedf00b3980c9b0327f8a5ac1104b8b86c711b0))

## [1.2.2](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.1...1.2.2) (2022-12-12)


### Bug Fixes

* added wait resource ([e2f4cca](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/e2f4cca736b2bbf49c0d2cae2bb217aaa2f69441))

## [1.2.1](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.2.0...1.2.1) (2022-12-12)


### Bug Fixes

* updated firewall name ([af56244](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/af562449c341838f0f0269f0007fc685ecc1c5b0))

# [1.2.0](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.1.1...1.2.0) (2022-12-12)


### Features

* added dynamic firewall option ([2c19f75](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/2c19f75cdf84ff151bbf1d099a4cec2699970a07))

## [1.1.1](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.1.0...1.1.1) (2022-12-12)


### Bug Fixes

* added datacenter option ([6c02266](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/6c02266031dfd49d03bdb4dfcfde774335f6e2d9))

# [1.1.0](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/compare/1.0.1...1.1.0) (2022-12-09)


### Bug Fixes

* updated pipeline ([4e0f499](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/4e0f49931b5a443f58a999e9ba64cdc196cc2b3c))


### Features

* added firewall to server ([d058247](https://gitlab.com/work4all/terraform-modules/terraform-hcloud-server/commit/d0582478e74e05315913847a368baef430db1f70))
