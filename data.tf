locals {
  user_data  = var.user_data != "" ? var.user_data : null
  datacenter = var.datacenter != "" ? var.datacenter : null
  ssh_keys   = var.ssh_keys != "" ? var.ssh_keys : null
  location   = var.datacenter == "" ? var.location : null
  labels = var.labels != {} ? var.labels : {
    "hcloud/node-group" : var.server_name
  }
}
