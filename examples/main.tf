terraform {
  required_version = ">= 1.2.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/52560002/terraform/state/server"
    lock_address   = "https://gitlab.com/api/v4/projects/52560002/terraform/state/server/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/52560002/terraform/state/server/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.38.1"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

module "network" {
  source           = "git::https://gitlab.com/hetzner-modules/network.git"
  network_ip_range = var.network_cidr
  network_name     = var.network_name
  subnet_ip_ranges = [var.network_ip_range]
}

module "server" {
  source      = "../"
  image_name  = var.image_name
  network_id  = module.network.network_id
  server_name = var.server_name
  server_type = var.server_type
}