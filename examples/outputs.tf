output "id" {
  description = "(int) Unique ID of the server."
  value       = module.server.id
}

output "ipv4_address" {
  description = "(string) The IPv4 address."
  value       = module.server.ipv4_address
}

output "network" {
  description = "(map) Private Network the server shall be attached to. The Network that should be attached to the server requires at least one subnetwork. Subnetworks cannot be referenced by Servers in the Hetzner Cloud API. Therefore Terraform attempts to create the subnetwork in parallel to the server. This leads to a concurrency issue. It is therefore necessary to use depends_on to link the server to the respective subnetwork"
  value       = module.server.network
}