variable "server_name" {
  type        = string
  description = "(Required, string) Name of the server to create (must be unique per project and a valid hostname as per RFC 1123)."
}

variable "server_type" {
  type        = string
  description = "(Required, string) Name of the server type this server should be created with."
}

variable "image_name" {
  type        = string
  description = "(Required, string) Name or ID of the image the server is created from. Note the image property is only required when using the resource to create servers. As the Hetzner Cloud API may return servers without an image ID set it is not marked as required in the Terraform Provider itself. Thus, users will get an error from the underlying client library if they forget to set the property and try to create a server."
}

variable "public_ips" {
  type        = bool
  description = "Whether the server gets assigned a public IP or not"
  default     = true
}

variable "backups" {
  type        = bool
  description = "Wether backups should be enabled or not"
  default     = false
}

variable "create_volume" {
  type        = bool
  description = "Whether volumes should be created on server creation"
  default     = false
}

variable "network_id" {
  type        = number
  description = "(Required) ID of the network"
}

variable "labels" {
  type        = map(string)
  description = "Labels to assign to server"
  default     = {}
}

variable "user_data" {
  type        = string
  description = "(Optional, string) Cloud-Init user data to use during server creation"
  default     = ""
}

variable "location" {
  type        = string
  description = "(Optional, string) The location name to create the server in. nbg1, fsn1, hel1 or ash"
  default     = "nbg1"
}

variable "datacenter" {
  type        = string
  description = "(Optional, string) The datacenter name to create the server in."
  default     = ""
}

variable "ssh_keys" {
  type        = list(string)
  description = "(Optional, list) SSH key IDs or names which should be injected into the server at creation time"
  default     = []
}

variable "volume_size" {
  type        = number
  description = "(Required, int) Size of the volume (in GB)."
  default     = 20
}

variable "automount" {
  type        = bool
  description = "(Optional, bool) Automount the volume upon attaching it (server_id must be provided)."
  default     = true
}

variable "volume_format" {
  type        = string
  description = "(Optional, string) Format volume after creation. xfs or ext4"
  default     = "ext4"
}

variable "delete_protection" {
  type        = bool
  description = "(Optional, bool) Enable or disable delete protection."
  default     = false
}
