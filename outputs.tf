output "id" {
  description = "(int) Unique ID of the server."
  value       = hcloud_server.this.id
}

output "name" {
  description = "(string) Name of the server."
  value       = hcloud_server.this.name
}

output "image" {
  description = "(string) Name or ID of the image the server was created from."
  value       = hcloud_server.this.image
}

output "ipv4_address" {
  description = "(string) The IPv4 address."
  value       = hcloud_server.this.ipv4_address
}

output "network" {
  description = "(map) Private Network the server shall be attached to. The Network that should be attached to the server requires at least one subnetwork. Subnetworks cannot be referenced by Servers in the Hetzner Cloud API. Therefore Terraform attempts to create the subnetwork in parallel to the server. This leads to a concurrency issue. It is therefore necessary to use depends_on to link the server to the respective subnetwork"
  value       = hcloud_server.this.network
}
